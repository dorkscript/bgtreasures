from django.conf.urls.defaults import *
from bgtreasures.views import *
from bgtreasures.models import StaticSitemap
from django.contrib.staticfiles.urls import staticfiles_urlpatterns 
from django.contrib import sitemaps

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

sitemaps = {
        'main':StaticSitemap
        }

urlpatterns = patterns('',
	# root pattern and old url pattern
	
	('^[Ii]ndex.html?$', home_old),
	('^$', home),
	
	#pressed flower art pattern
	('^[Gg]allery.html?$', pressed_flower_art_old),
	('^pressed-flower-art$', pressed_flower_art),
	
	
	#other products pattern
	('^[Oo]ther_[Pp]roducts.html?$', other_products_old),
	('^other-products$', other_products),
	
	
	#shows pattern
	('^[Ss]hows.html?$', shows_old),
	('^shows$', shows),
	
	
	#about pattern
	('^[Aa]bout.html?$', about_old),
	('^about$', about),
	
	
	#contact pattern
	('^[Cc]ontact.html?$', contact_old),
	('^contact$', contact),

	#(r'^sitemap\.xml?', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps})
	
	
    # Examples:
    # url(r'^$', 'bgtreasures.views.home', name='home'),
    # url(r'^bgtreasures/', include('bgtreasures.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += staticfiles_urlpatterns()
