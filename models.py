
from django.core.urlresolvers import reverse
from django.contrib.sitemaps import Sitemap
from datetime import datetime


class AbstractSitemapClass():
    changefreq = 'daily'
    url = None
    def get_absolute_url(self):
        return self.url

class StaticSitemap(Sitemap):
	#TODO: come up with a smarter way to load this
    pages = {
             'pressed-flower-art':'/pressed-flower-art',
             'other-products':'/other-products',
             'shows':'/shows',
             'contact':'/contact',
             'about':'/about',
             'home':'/',
             }

    main_sitemaps = []
    for page in pages.keys():
        sitemap_class = AbstractSitemapClass()
        sitemap_class.url = pages[page]        
        main_sitemaps.append(sitemap_class)

    def items(self):
        return self.main_sitemaps    
    
    priority = 1
    changefreq = "monthly" 

class GalleryImage:
    def __init__(self, imgt = None, img = None, alt = None, desc = None):
        self.imgt = imgt
        self.img = img
        self.alt = alt
        self.desc = desc

class Show:
	def __init__(self, date = None, end_date = None, event = None, location = None, link = None, link_text = None):
		self.date = date
		self.end_date = end_date
		self.event = event
		self.location = location
		self.link = link
		self.link_text = link_text

		self.date_text = self.get_date_text()
	
	def get_date_text(self):
		str = self.date
		if self.end_date != None:
			str += ("-%s" % (self.end_date))

		return str