from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template import RequestContext

from django import http
from models import Show, GalleryImage

#c = RequestContext(request)

def home(request):
	imgs = []
	imgs.append(GalleryImage(img = "images/69.jpg", imgt = "images/69t.jpg", alt="#69-Silk Roses", desc="#69-Silk Roses - Click on 'Pressed Flower Art' for more information."))
	imgs.append(GalleryImage(img = "images/67.jpg", imgt = "images/67t.jpg", alt="#67-Autumn Windmill Landscape", desc="#67-Autumn Windmill Landscape - Click on 'Pressed Flower Art' for more information."))
	imgs.append(GalleryImage(img = "images/70c.jpg", imgt = "images/70Ct.jpg", alt="#70-A Simple Bouquet Coaster", desc="Ceramic Coasters featuring #70-A Simple Bouquet - Click on 'Pressed Flower Art' for more information."))
	imgs.append(GalleryImage(img = "images/58.jpg", imgt = "images/58t.jpg", alt="#58-Custom Bridal Invitation", desc="#58-Custom Bridal Invitation - Click on 'Pressed Flower Art' for more information on custom work."))
	imgs.append(GalleryImage(img = "images/63.jpg", imgt = "images/63t.jpg", alt="Pastel Bouquet, matted & framed 8x10, original, digital copy", desc="#63-Pastel Bouquet - Click on 'Pressed Flower' Art for more information."))
	imgs.append(GalleryImage(img = "images/WRP-Brn-Floral.jpg", imgt = "images/WRPt-Brn-Floral.jpg", alt="Herbal Heating and Cooling Wrap 30 in.", desc="Herbal Heating and Cooling Wrap - Click on 'Other Products' for more information."))
	imgs.append(GalleryImage(img = "images/CATNIP.jpg", imgt = "images/CATNIPt.jpg", alt="Catnip Sacks", desc="Catnip Sacks made with 100% catnip and fun fabrics - Click on 'Other Products' for more information"))
	return render_to_response('home.html', {"imgs": imgs}, context_instance=RequestContext(request))

def pressed_flower_art(request):
	return render_to_response('pressed_flower_art.html', {}, context_instance=RequestContext(request))

def other_products(request):
	return render_to_response('other_products.html', {}, context_instance=RequestContext(request))

def shows(request):
	shows = []
	shows.append(Show(date = "6/9/2012", end_date = "6/10/2012", event = "Keuka Arts Festival", location = "Penn Yan, NY", link = "http://www.keukaartsfestival.com", link_text = "www.keukaartsfestival.com"))
	shows.append(Show(date = "8/4/2012", event = "Cortlant Arts & Wine Festival", location = "Cortland, NY", link = "http://www.cortlandartsandwine.org", link_text = "www.cortlandartsandwine.org"))
	shows.append(Show(date = "9/29/2012", end_date = "9/30/2012", event = "Hunt Country Harvest Festival", location = "Branchport, NY", link = "http://www.huntwines.com", link_text = "www.huntwines.com"))
	shows.append(Show(date = "11/9/2012", end_date = "11/11/2012", event = "Christkindle at the Granger Homestead (tentative)", location = "Canandaigua, NY", link = "http://www.canandaiguachristkindlmarket.com", link_text = "www.canandaiguachristkindlmarket.com"))

	shows.append(Show(date = "4/2/2011", end_date = "4/3/2011", event = "41st Annual Central New York Maple Festival. 9 a.m. to 5 p.m. Saturday; 9 a.m. to 4 p.m. Sunday.", location = "Marathon, NY", link = "http://www.maplefest.org", link_text = "www.maplefest.org"))
	shows.append(Show(date = "8/6/2011", event = "Cultural Council of Cortland County Arts and Wine Festival", location = "Cortland, NY", link = "http://www.cortlandartsandwine.com", link_text = "www.cortlandartsandwine.com"))
	shows.append(Show(date = "9/20/2011", end_date = "9/21/2011", event = "DeSales Fall Arts & Crafts Show", location = "DeSales High School, 90 Pulteney St., Geneva, NY"))
	shows.append(Show(date = "10/1/2011", event = "22nd Annual Gourmet Harvest Festival and Art Show", location = "Hunt Country Winery, Branchport, NY", link = "http://www.huntwines.com", link_text = "www.huntwines.com"))
	shows.append(Show(date = "11/12/2011", event = "34th Annual Experiment Station Holiday Art & Craft Show", location = "Jordan Hall, Geneva, NY"))
	#shows.append(Show(date = "", event = "", location = "", link = "http://", link_text = ""))

	return render_to_response('shows.html', {"data": shows}, context_instance=RequestContext(request))

def about(request):
	return render_to_response('about.html', {}, context_instance=RequestContext(request))

def contact(request):
	return render_to_response('contact.html', {}, context_instance=RequestContext(request))




"""
Old URLS with Permanent Redirects to current url
"""

def home_old(request):
	return http.HttpResponsePermanentRedirect('/')

def pressed_flower_art_old(request):
	return http.HttpResponsePermanentRedirect('pressed-flower-art')

def other_products_old(request):
	return http.HttpResponsePermanentRedirect('other-products')
	
def shows_old(request):
	return http.HttpResponsePermanentRedirect('shows')
	
def about_old(request):
	return http.HttpResponsePermanentRedirect('about')
	
def contact_old(request):
	return http.HttpResponsePermanentRedirect('contact')